#!/bin/bash
# Subject: create a script to flash the switch port connected to the computer running this script
# Author: Scott Campbell
# Date: February 3, 2020
# Version: 1.10

# Note: this command will have to be run with root privelages to be able to shut down and turn on the interface
# Update: ifdown and ifup are deprecated so I opted to use the ip command instead

# Let's try to get the local interface name by using the ip addr command to list the interfaces, then
# use awk to find any strings that have the pattern inet and then brd, print the last field and exit
interfacename="$( ip addr show | awk '/inet.*brd/{print $NF; exit}')"

# We will trap the ctrl-c function in case the interface is toggled off, we want to leave it on at the end
# of the script

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
#        echo "** Trapped CTRL-C"
        upcmd 
        exit
}

function downcmd() {
#	echo $interfacename
ip link set $interfacename down
}

function upcmd() {
#	echo $interfacename
ip link set $interfacename up
}

echo -e "\nRun as root user.\n"

# Note the -e flag enables interpretation of backslash escapes so we can get newlines
echo -e "The network interface we will be toggling is called: $interfacename\n\n"

# Now we have the interface name we will use our downcmd and upcmd functions to bring down or
# bring up the interface

while true ; do
   downcmd
  read -rep $'Turning interface OFF, waiting 5 seconds\n' -t 5
   upcmd
  read -rep $'Turning interface ON, waiting 5 seconds\n' -t 5
done
