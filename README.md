# Flashing switch port

A simple script you can run from a linux terminal to toggle your network interface on and off, making it easy to find which port on a switch your computer is connected to.